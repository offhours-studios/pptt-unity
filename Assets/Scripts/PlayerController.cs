﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public Transform playerOne;
    public Transform table;

    Vector3 mousePosition;
    Vector3 tablePosition;
    Vector3 playerPosition;

    // Update is called once per frame
    void Update()
    {
        tablePosition = table.position;
        playerPosition = playerOne.position;
        mousePosition = Input.mousePosition;

        mousePosition.z = 6.0f;

        Vector3 newPos = Camera.main.ScreenToWorldPoint(mousePosition);
        newPos.z = 1.3f;
        newPos.y = -2.584f + (mousePosition.y / 100.0f);
        newPos.x = -1.584f + (mousePosition.x / 200.0f);

        playerOne.position = newPos;
        if (newPos.y < -1.084f)
        {
            newPos.y = -1.084f;
            playerOne.position = newPos;
        }
        if (newPos.x < 0.2f)
        {
            //var angles = playerOne.rotation.eulerAngles;
            //angles.z = 500f * newPos.x;
            //angles.x = newPos.x;
            //angles.y = newPos.y;
            //playerOne.rotation = Quaternion.Euler(0, 0, angles.z);
            playerOne.Rotate(Vector3.forward, 100.0f * Time.deltaTime, Space.Self);
        }
    }
}